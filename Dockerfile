FROM node:alpine AS base

RUN apk add -U tzdata
RUN mkdir -p /var/log/services

#
# Build dependencies
FROM base AS build-dependencies

# install dependencies to git submodule
RUN apk update && apk add --update git openssh tree
RUN npm install -g typescript

#
# Build
FROM build-dependencies AS build
WORKDIR /opt
COPY . /opt/

RUN git submodule update --init
RUN npm install

# produces a dist folder
RUN tsc --build

#
# Release
FROM base AS release
WORKDIR /opt

COPY package.json package-*.json /opt/
RUN npm install --production

# copy in distribution code
COPY --from=build /opt/dist /opt

CMD ["node", "main.js"]